import React, { FC, ReactElement, useEffect, useState } from 'react';
import { getPosts, Post } from '../../Endpoints/Posts/Posts';
import { getComments, Comment } from '../../Endpoints/Comments/Comments';
import { PostComponent } from '../../Components/Post/PostComponent';

export const Feed: FC = (): ReactElement => {

    const [ posts, setPosts ] = useState<Post[]>([]);
    const [ comments, setComments ] = useState<Comment[]>([]);

    useEffect(() => {
        getPosts()
        .then( (posts: Post[]) => {
            getComments()
            .then( (comments: Comment[]) => {
                setComments(comments);
                setPosts(posts);
            });
        });
    }, []);

    const getCommentsForPost = (postId: Number): Comment[] => comments.filter(comment => comment.postId === postId);

    return (
        <div className='feed-container'>
            <h1>Feed Page</h1>
            <div className='posts-container'>
                { posts.map( post => <PostComponent key={post.id as number} post={post} comments={getCommentsForPost(post.id)} /> )}
            </div>
        </div>
    )
};