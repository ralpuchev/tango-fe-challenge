import React, { FC, ReactElement } from 'react';
import { Comment } from '../../Endpoints/Comments/Comments';

import './CommentComponent.scss';

export interface CommentProps {
    comment: Comment
}

export const CommentComponent: FC<CommentProps> = ({ comment }: CommentProps): ReactElement => {

    return (
        <div className='comment-component-container'>
            <div className='comment-header'>
                <span>{ comment.name }</span>
                <span>{ comment.email }</span>
            </div>
            <div className='comment-body'>
                <span>{ comment.body }</span>
            </div>
        </div>
    )
};