import React, { FC, ReactElement, useState } from 'react';
import { Post } from '../../Endpoints/Posts/Posts';
import { Comment } from '../../Endpoints/Comments/Comments';
import { CommentComponent } from '../Comment/CommentComponent';

import './PostComponent.scss';

export interface PostProps {
    post: Post,
    comments: Comment[]
}

export const PostComponent: FC<PostProps> = ({ post, comments }: PostProps): ReactElement => {

    const [ showComments, setShowComments ] = useState<Boolean>(false);

    const toggleShowComments = () => setShowComments(!showComments);

    return (
        <div className='post-component-container'>
            <div className='post-body'>
                <h2>{ post.title }</h2>
                <span>{ post.body }</span>
            </div>
            {
                <div className='post-comments-container' onClick={toggleShowComments}>
                    <span className='comments-number'>Comment ({comments.length})</span>
                    {showComments && (
                        comments.map( comment => <CommentComponent key={`${post.id}-${comment.id}`} comment={comment} />)
                    )}
                </div>
                
            }
        </div>
    )
};