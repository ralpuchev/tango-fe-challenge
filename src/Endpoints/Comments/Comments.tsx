import axios from "axios"
import { COMMENTS_URL } from '../constants';

export type Comment = {
    id: Number,
    postId: Number,
    body: String,
    email: String,
    name: String
}

export const getComments = (): Promise<any> => {
    return new Promise((resolve,reject) => {
        axios.get(COMMENTS_URL)
        .then( response => {
            resolve(response.data as Comment[]);
        })
        .catch( error => {
            console.log('getComments error: ', error);
            reject(null);
        })
    });
}