import axios from "axios"
import { POSTS_URL } from '../constants';

export type Post = {
    id: Number,
    userId: Number,
    body: String,
    title: String
}

export const getPosts = (): Promise<any> => {
    return new Promise((resolve,reject) => {
        axios.get(POSTS_URL)
        .then( response => {
            resolve(response.data as Post[]);
        })
        .catch( error => {
            console.log('getPosts error: ', error);
            reject(null);
        })
    });
}